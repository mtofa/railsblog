require 'rails_helper'

RSpec.feature "User Sign in" do

before do
  @user1= User.create(email: "gggg@b.com", password: "abcd2123")
end

  scenario "Signin with valid credentials" do

  visit "/"

  click_link "Sign in"
  fill_in "Email", with: @user1.email
  fill_in "Password", with: @user1.password
  click_button "Log in"

  expect(page).to have_content("Signed in successfully.")
  expect(page).to have_content("Signed in as #{@user1.email}")



  end


end
