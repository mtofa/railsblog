require 'rails_helper'

RSpec.feature "Editing an Article" do

  
  before do

    john= User.create!(email: "a@b.com",password: "asdfg1234")
    login_as(john)
    @article = Article.create(title:"First article", body: "Article1 Body", user: john)

  end

  scenario "User Edit an Article" do

    visit "/"

    click_link @article.title
    click_link "Edit Article"

    fill_in "Title", with: "Update first Article"
    fill_in "Body",  with: "Update Article Body"

    click_button "Update Article"

    expect(page).to have_content("Article has been updated")
    expect(page.current_path).to eq(article_path(@article))
  
   end



  scenario "User fail to edit an Article" do

    visit "/"

    click_link @article.title
    click_link "Edit Article"

    fill_in "Title", with: ""
    fill_in "Body",  with: "Update Article Body"

    click_button "Update Article"

    expect(page).to have_content("Article has not been updated")
    expect(page.current_path).to eq(article_path(@article))
  
   end


end