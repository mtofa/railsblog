require 'rails_helper'

RSpec.feature "Creating Articles" do


  before do

   john= User.create!(email: "a@b.com",password: "asdfg1234")
   login_as(john)
  
  end

  scenario "A user creates an Article" do
  
    visit "/"
    click_link "New Article"
    fill_in "Title", with: "First Article"
    fill_in "Body", with: "Test body"
    click_button "Create Article"
    expect(page).to have_content("Article has been created!")
    expect(page.current_path).to eq(articles_path)

    #expect(page).to have_content("Created by #{@john.email}")
  end

  scenario "A user fail to create a new article" do
  
    visit "/"
    click_link "New Article"
    fill_in "Title", with: ""
    fill_in "Body", with: ""
    click_button "Create Article"
    expect(page).to have_content("Title cannot be blank!")
    expect(page).to have_content("Body cannot be blank!")
    #expect(page.current_path).to eq(articles_path)
  end


end
