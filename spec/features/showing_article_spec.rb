require 'rails_helper'

RSpec.feature "Showing Articles" do


  before do
    @john= User.create!(email: "a@b.com",password: "asdfg1234")
    login_as(@john)
    @jonh_article = Article.create(title:"First article", body: "Article1 Body",user:@john)
    visit "/"
    click_link('SignOut')

    @alex= User.create!(email: "w@c.com",password: "asdfg1234")
    login_as(@alex)
    @alex_article2 = Article.create(title:"Second article", body: "Article1 Body",user:@alex)
    visit "/"
    click_link('SignOut')

    #@jonh_article2 = Article.create(title:"Secound article", body: "Article2 Body")

  end

  scenario "Display Article" do

  visit "/"
  click_link @jonh_article.title

  expect(page).to have_content(@jonh_article.title)
  expect(page).to have_content(@jonh_article.body)
  expect(current_path).to eq(article_path(@jonh_article)) 
  
  end

  scenario "A non login user cannot see the Edit/Delete link" do

  visit "/"
  click_link @jonh_article.title
  expect(page).to have_content(@jonh_article.title)
  expect(page).to have_content(@jonh_article.body)
  expect(current_path).to eq(article_path(@jonh_article)) 
  
  expect(page).not_to  have_link("Edit Article")
  expect(page).not_to  have_link("Delete Article")

  end

  scenario "Non-owner cannot see edit/delete link" do

  login_as(@alex)

  visit "/"
  click_link @jonh_article.title
  expect(page).to have_content(@jonh_article.title)
  expect(page).to have_content(@jonh_article.body)
  expect(current_path).to eq(article_path(@jonh_article)) 
  
  expect(page).not_to  have_link("Edit Article")
  expect(page).not_to  have_link("Delete Article")

  end

  scenario "Owner can see edit/delete link" do

  login_as(@john)

  visit "/"
  click_link @jonh_article.title
  expect(page).to have_content(@jonh_article.title)
  expect(page).to have_content(@jonh_article.body)
  expect(current_path).to eq(article_path(@jonh_article)) 
  
  expect(page).to  have_link("Edit Article")
  expect(page).to  have_link("Delete Article")

  end


end