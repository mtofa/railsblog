require 'rails_helper'

RSpec.feature "Delete an Article" do

  
  before do


    john= User.create!(email: "a@b.com",password: "asdfg1234")
    login_as(john)

    @article = Article.create(title:"First article", body: "Article1 Body",user: john)

  end

  scenario "User Delete an Article" do

    visit "/"

    click_link @article.title
    click_link "Delete Article"

    #fill_in "Title", with: "Update first Article"
    #fill_in "Body",  with: "Update Article Body"

    #click_button "yes"

    expect(page).to have_content("Article has been deleted")
    expect(page.current_path).to eq(articles_path)
  
   end



 


end