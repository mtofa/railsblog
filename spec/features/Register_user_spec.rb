require 'rails_helper'

RSpec.feature "Users Register" do

 scenario "With valid credentials" do

 visit "/"
 #save_and_open_page
 click_link "Register"
 fill_in "Email", with: "a@b.com"
 fill_in "Password", with: "abcd@123"
 fill_in "Password confirmation", with: "abcd@123"
 click_button "Sign up"

 expect(page).to have_content("You have signed up successfully.")

 end

end