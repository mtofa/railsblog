require 'rails_helper'

RSpec.feature "Add comment to an article" do

before do
    @john= User.create!(email: "a@b.com",password: "asdfg1234")
    @alex= User.create!(email: "w@c.com",password: "asdfg1234")
    @jonh_article = Article.create(title:"First article", body: "Article1 Body",user:@john)
end

 scenario "Permit a signin user to write a review" do

  login_as(@john)
  visit "/"
  click_link @jonh_article.title

  fill_in "New Comment", with: "First comment"
  click_button "Add Comment"

  expect(page).to have_content("Comment has been created")
  expect(page).to have_content("First comment")
  
  expect(current_path).to eq(article_path(@jonh_article.id))
  #expect(current_path).to eq(article_path(@article.comments.last.id))

  end



end