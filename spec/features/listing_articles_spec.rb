require 'rails_helper'

RSpec.feature "Listing Articles" do

  
  before do

    john= User.create!(email: "a@b.com",password: "asdfg1234")
    login_as(john)

    @article1 = Article.create(title:"First article", body: "Article1 Body",user:john)
    @article2 = Article.create(title:"Secound article", body: "Article2 Body",user:john)
    visit "/"
    click_link('SignOut')

  end

  scenario "List all Articles" do

    visit "/"
    
    expect(page).to have_content(@article1.title)
    expect(page).to have_content(@article1.body)
    expect(page).to have_content(@article2.title)
    expect(page).to have_content(@article2.body)

    expect(page).to have_link(@article1.title)
    expect(page).to have_link(@article2.title)
    expect(page).not_to have_link("New Article")
  
   end


end