require 'rails_helper'

RSpec.feature "Signout user" do

before do
  @user1= User.create(email: "gggg@b.com", password: "abcd2123")
  visit "/"
  click_link "Sign in"
  fill_in "Email", with: @user1.email
  fill_in "Password", with: @user1.password
  click_button "Log in"
end


  scenario "Sign out user" do

  visit "/"
  click_link "SignOut"
  expect(page).to have_content("Signed out successfully.");

  end

end