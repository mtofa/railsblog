class CommentsController < ApplicationController

	def create
		@article=Article.find(params[:article_id])
		@comment= @article.comments.build(comment_params)
		@comment.user= current_user
		if @comment.save
			flash[:success]= "Comment has been created"
			else
			flash.now[:danger]="Comment not saved"
		end

        redirect_to article_path(@article)

	end 

	private

	def comment_params

        params.require(:comment).permit(:comment_body)
	end
end
