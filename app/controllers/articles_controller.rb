class ArticlesController < ApplicationController
  
  before_action :authenticate_user!, except: [:index,:show]

  def index
  
    @article=Article.all.sorted
   
  end

  def edit
    @article=Article.find(params[:id])
  end

  def update
  
    @article=Article.find(params[:id])
    if @article.update(article_params)
    flash[:success] = "Article has been updated"
    redirect_to article_path
    else
    flash.now[:danger]="Article has not been updated"
    render :edit
    end


  end

  def show
  
    @article=Article.find(params[:id])
    @comment= @article.comments.build
  end

  def new
    @article=Article.new

  end

  def create
    @article = current_user.articles.build(article_params)
    
   # @article = Article.new(article_params)
   # @article.user_id = current_user.id


    if @article.save
       flash[:success] = "Article has been created!"
       redirect_to articles_path
    else
       #flash[:danger]="Article has not been created"
       #redirect_to articles_path
       render :new
    end

  end

  def destroy

    
    @article = Article.find(params[:id])


    #raise current_user.id.inspect
    #raise @article.user.id.inspect

  if (@article.user.id==current_user.id)

    if @article.destroy
      flash[:success] = "Article has been deleted"
      redirect_to articles_path
    end
  else
      flash[:danger] = "Article has not deleted"
      redirect_to root_path

  end



   end


  private

    def article_params
      params.require(:article).permit(:title,:body)
    end


end
