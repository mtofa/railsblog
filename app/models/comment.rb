class Comment < ActiveRecord::Base
  belongs_to :article
  belongs_to :user

  validates :comment_body, presence: true

  def self.persisted
    where.not(id: nil)
  end

end
