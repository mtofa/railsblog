class Article < ActiveRecord::Base
	belongs_to  :user
	has_many    :comments, dependent: :destroy
	validates :title, presence: {message: "Title cannot be blank!"}
	validates :body,  presence: {message: "Body cannot be blank!"}

	scope :sorted, lambda{order('title ASC')}


end
